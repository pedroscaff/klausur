#ifndef VERTRAG
#define VERTRAG

#include "Edge.h"

class Vertrag : public Edge
{

public:

    // Preise fuer verschiedene Stueckanzahlen
    Vertrag(Node& rSrc, Node& rDst, int price_1, int price_100, int price_1000);

    // Kantengewicht 
    virtual double getWeight(int stuecke);

    double getPrice(int stuecke)  { return getWeight(stuecke); }

private:

    double m_price_total, m_price_1, m_price_100, m_price_1000;

};


#endif