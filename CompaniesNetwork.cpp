#include "CompaniesNetwork.h"
#include "Vertrag.h"

void CompaniesNetwork::addVertrag(Node& rFirma1, Node& rFirma2, double preis_1, double preis_100, double preis_1000)
{
    addEdge(new Vertrag(rFirma1, rFirma2, preis_1, preis_100, preis_1000));
}

void CompaniesNetwork::saveOriginDestNodes()
{
	// get Start und End Nodes by ID
    m_Origin = getNodeById("SysEngGmbH");
    m_Dest = getNodeById("HWTestingAG");
}

Node* CompaniesNetwork::getNodeById(const std::string& rNodeId)
{
    for (std::list<Node*>::iterator it = m_nodes.begin(); it != m_nodes.end(); it++) {
        if ((*it)->getID() == rNodeId)
            return *it;
    }
    return NULL;
}

/*
 * Dijkstra-Algorithmus (vgl. https://en.wikipedia.org/wiki/Dijkstra%27s_algorithm)
 */
bool CompaniesNetwork::findShortestPathDijkstraModified(std::deque<Edge*>& rPath, int num_Platinen)
{
	// get origin und dest Nodes (sind immer gleich)
	Node *pSrcNode = getOrigin();
	Node *pDstNode = getDest();
	tNodeList Q;				// Liste der noch zu betrachtenden Knoten
	tDijkstraMap nodeEntry;		// Datenstruktur für prevNode, prevEdge, dist (für jeden Node)

	//Initialisierung
	for (tNodeList::iterator it = m_nodes.begin(); it != m_nodes.end(); it++)
	{
		Node* pCurrNode = *it;
		Q.push_back(pCurrNode);

		tDijkstraEntry entry;
		entry.dist = 10000000;
		entry.prevNode = NULL;
		entry.prevEdge = NULL;

		nodeEntry[pCurrNode] = entry;
	}

	nodeEntry[pSrcNode].dist = 0;

	// Start des Algorithmus
	while (!Q.empty())
	{
		// Finde Node mit der kleinsten gespeicherten Distanz
		Node* pCurrentNode = Q.front();
		for (tNodeList::iterator it = Q.begin(); it != Q.end(); it++)
		{
			if (nodeEntry[pCurrentNode].dist > nodeEntry[*it].dist)
			{
				pCurrentNode = *it;
			}
		}
		tDijkstraEntry& rCurrNodeEntry = nodeEntry[pCurrentNode];
		// Aktuellen Knoten aus der Liste der zu betrachtenden Knoten entfernen
		Q.remove(pCurrentNode);

		// Falls der aktuelle Knoten mit der kleinsten Distanz der Zielknoten ist -> abbrechen (fertig)
		if (pCurrentNode == pDstNode)
		{
			break;
		}

		std::list<Edge*>& rOutEdges = pCurrentNode->getOutgoingEdges();

		// Alle ausgehenden Kanten des aktuellen Knotens durchsuchen
		for (std::list<Edge*>::iterator it = rOutEdges.begin(); it != rOutEdges.end(); it++)
		{
			// cast to use Vertrag class and modified getWeigth() function
			Vertrag* pVertrag = dynamic_cast<Vertrag*>(*it);
			Node* pNeighbour = &(pVertrag->getDstNode());

			tDijkstraEntry& rNeighbourEntry = nodeEntry[pNeighbour];

			double newDistance = rCurrNodeEntry.dist + pVertrag->getWeight(num_Platinen);
			if (newDistance < rNeighbourEntry.dist)
			{
				rNeighbourEntry.dist = newDistance;
				rNeighbourEntry.prevNode = pCurrentNode;
				rNeighbourEntry.prevEdge = *it;
			}
		}
	}

	// Den Weg in 'rPath' speichern.
	for (Node* n = pDstNode; n != NULL; n = nodeEntry[n].prevNode)
	{
		if (nodeEntry[n].prevEdge != NULL)
		{
			rPath.push_front(nodeEntry[n].prevEdge);
		}
	}

	return !rPath.empty();
}