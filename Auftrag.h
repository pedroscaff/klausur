#ifndef AUFTRAEGE_H
#define AUFTRAEGE_H

#include "Node.h"
#include <string>

/*
 * Ein Struct zur Speicherung eines Auftrags.
 */
struct Auftrag
{
    std::string herstellung;
    std::string bestueckung;
    std::string test;
    double m_price;
};


#endif