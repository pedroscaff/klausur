#include <iostream>
#include <sstream>

#include "AuftragGUI.h"
#include "Vertrag.h"


//---------------------------------------------------------------------------------------------------------------------

// returns the choosen Menu-Point
int AuftragGUI::showMenu()
{
    std::cout << "Bitte wählen Sie eine Zahl für die jeweilige Aktion:" << std::endl
        << "1 - Neue Buchung" << std::endl
        << "2 - Ausgabe aller Umsaetzen" << std::endl
        << "3 - Beenden" << std::endl;
        
    std::cout << "Eingabe: ";
    int eingabe;
    std::cin >> eingabe;
    return eingabe;
}


//---------------------------------------------------------------------------------------------------------------------

// gibt true zurück, wenn die vom Nutzer gewählte Route gebucht werden soll.
void AuftragGUI::neuAuftrag()
{
    std::cout << "Bitte Anzahl der Platinen eingeben: " << std::endl;
    int num_Platinen;
    std::cin >> num_Platinen;
    if (0 >= num_Platinen) {
        std::cout << "falsche Eingabe" << std::endl;
        return;
    }
    Graph::tPath path;
    // finde die billigsten Vertraegen und speichere das Ergebnis in path:
    m_rNetwork.findShortestPathDijkstraModified(path, num_Platinen);

    // neuer Auftrag
    Auftrag a;
    // speichere Auftraege
    std::stringstream s;
    // s << "Node_" << setw(4) << setfill('0') << s_numInstances;
    // m_id = s.str();

    int it_number = 0;

    std::cout << "\n--------------------------------------------------------------" << std::endl;
    // Gefundene Strecke durchlaufen
    for (std::deque<Edge*>::iterator it = path.begin(); it != path.end(); it++)
    {
        // (dynamic_cast gibt NULL zurück, wenn der Cast von der Basisklasse zur abgeleiteten Klasse ungültig ist!)
        if ( Vertrag* pVertrag = dynamic_cast<Vertrag*>(*it) ) {

            // save strings im Auftrag variable und ausgabe
            if(0 == it_number) {
                s << "Herstellung: " << pVertrag->toString() << "; Preis: " 
                    << pVertrag->getPrice(num_Platinen) << std::endl;
                std::cout << s.str();
                a.herstellung = s.str();
            }
            else if(1 == it_number) {
                s << "Bestueckung: " << pVertrag->toString() << "; Preis: " 
                    << pVertrag->getPrice(num_Platinen) << std::endl;
                std::cout << s.str();
                a.bestueckung = s.str();
            }
            else if(2 == it_number) {
                s << "Test: " << pVertrag->toString() << "; Preis: " 
                    << pVertrag->getPrice(num_Platinen) << std::endl;
                std::cout << s.str();
                a.test = s.str();
            }
            it_number++;
            // clear stringstream
            s.str(std::string());
            // addiere preis
            a.m_price += pVertrag->getPrice(num_Platinen);
        }
    }
    std::cout << "Gesamtpreis: " << a.m_price << std::endl;
    std::cout << "\n--------------------------------------------------------------" << std::endl;
    // add auftrag zu Liste
    m_auftraege.push_back(a);
}


//---------------------------------------------------------------------------------------------------------------------

void AuftragGUI::showAuftraegeList()
{
    std::cout << "\n--------------------------------------------------------------" << std::endl;

    for (std::list<Auftrag>::iterator it = m_auftraege.begin(); it != m_auftraege.end(); it++)
    {    
        std::cout << it->herstellung << std::endl;
        std::cout << it->bestueckung << std::endl;
        std::cout << it->test << std::endl;
        std::cout << "Gesamtpreis: " << it->m_price;

        std::cout << "\n--------------------------------------------------------------" << std::endl;
    }
}


//---------------------------------------------------------------------------------------------------------------------