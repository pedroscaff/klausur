#include "Vertrag.h"

Vertrag::Vertrag(Node& rSrc, Node& rDst, int price_1, int price_100, int price_1000) : 
Edge(rSrc, rDst),
m_price_1(price_1), m_price_100(price_100),m_price_1000(price_1000) 
{ }

double Vertrag::getWeight(int stuecke)
{ 
	if (stuecke < 100)
	{
		m_price_total = stuecke * m_price_1;
	}
	else if (stuecke > 100 && stuecke < 1000)
	{
		m_price_total = stuecke * m_price_100;
	}
	else
	{
		m_price_total = stuecke * m_price_1000;
	}

	return m_price_total;
}