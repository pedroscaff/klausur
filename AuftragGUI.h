#ifndef AUFTRAG_GUI
#define AUFTRAG_GUI

#include "CompaniesNetwork.h"
#include "Auftrag.h"

class AuftragGUI
{

public:

    // CompaniesNetwork wird als Referenz in der Klasse gespeichert, damit sie damit arbeiten kann.
    AuftragGUI(CompaniesNetwork& rNetwork) : m_rNetwork(rNetwork) { }

    // zeigt ein Hauptmenü an.
    // Rückgabewert: der vom Nutzer gewählte Menüpunkt
    int showMenu();

    // Neuer Auftrag
    void neuAuftrag();

    // Gibt eine Liste aller Auftraege aus.
    void showAuftraegeList();


private:

    std::list<Auftrag> m_auftraege;

    CompaniesNetwork& m_rNetwork;
};

#endif