#ifndef COMPANIES_NETWORK
#define COMPANIES_NETWORK

#include "Graph.h"


class CompaniesNetwork : public Graph
{

public:

    // kleine Hilfsfunktion, um einen Vertrag schneller einzutragen
    void addVertrag(Node& rFirma1, Node& rFirma2, double preis_1, double preis_100, double preis_1000);

    void saveOriginDestNodes();

    //Start Node ist immer SysEng GmbH
    Node* getOrigin() { return m_Origin; };

    //End Node ist immer HWTesting AG
    Node* getDest() { return m_Dest; };

    // Nodes anhand ihrer ID abfragen. (wird für die Nutzereingabe benötigt)
    Node* getNodeById(const std::string& rNodeId);

    // Dijkstra mit verschiedenen Argumenten
    bool findShortestPathDijkstraModified(std::deque<Edge*>& rPath, int num_Platinen);

private:

	Node *m_Origin, *m_Dest;
    // Graph::addEdge wird private. 
    // Es soll 'von außen' nur noch addVertrag verwendet werden.
    using Graph::addEdge; 

};


#endif