#include <iostream>

#include "CompaniesNetwork.h"
#include "AuftragGUI.h"

int main()
{

	CompaniesNetwork vertraege;

	Node& rOrigin = vertraege.addNode(new Node("SysEngGmbH"));
	Node& rEndos = vertraege.addNode(new Node("EndosGmbH"));
	Node& rLightOff = vertraege.addNode(new Node("LightOffGmbH"));
	Node& rLPBes = vertraege.addNode(new Node("LPBesAG"));
	Node& rBP = vertraege.addNode(new Node("BPCoKG"));
	Node& rHWTesting = vertraege.addNode(new Node("HWTestingAG"));

	vertraege.saveOriginDestNodes();

	vertraege.addVertrag(rOrigin, rEndos, 200, 80, 20);
	vertraege.addVertrag(rOrigin, rLightOff, 160, 120, 70);
	vertraege.addVertrag(rEndos, rLPBes, 180, 100, 30);
	vertraege.addVertrag(rLightOff, rLPBes, 160, 80, 20);
	vertraege.addVertrag(rLightOff, rBP, 170, 120, 20);
	vertraege.addVertrag(rLPBes, rHWTesting, 300, 260, 210);
	vertraege.addVertrag(rBP, rHWTesting, 290, 280, 200);

	AuftragGUI gui(vertraege);	

	vertraege.saveAsDot("graph.dot");
	
	bool bRunning = true;
    while (bRunning)
    {
        int menuEntry = gui.showMenu();

        // neuer Auftrag
        if (menuEntry == 1) 
        {
        	gui.neuAuftrag();
        }
        // Liste aller Auftraege anzeigen
        else if (menuEntry == 2) 
        {
            gui.showAuftraegeList();
        }
        // Schleife verlassen und Programm beenden
        else if (menuEntry == 3)
        {
            bRunning = false; 
        }
        // falsche Eingabe
        else 
        {
            std::cout << "Fehlerhafte Eingabe! Bitte wiederholen ..." << std::endl << std::endl;
        }
    }

	return 0;
}
